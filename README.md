# Simple quiz

Made in C language

## Description
Select one of few categories to play and answer the questions correctly to earn points. 
By default there are four answers to each question where only one is correct. 
Correct answer gives you 3 points, while incorrect reduces 1 point.
In general, correct answer gives (number_of_answers - 1) points. That is to make sure that expected points gained on 
each question while guessing randomly is exactly zero.

## Usage
Source files are provided in this repository if you wish to compile the quiz yourself. Otherwise an .exe file is 
provided for Windows users. For this project to work, question files must be present in the same folder where the 
quiz is located. Those files are included with the project. 

You can add or remove questions from the files. An explanation on how to do it properly can be found in the 
beginning of those files. Maximum number of questions is 85.

Changing amount of answers is also possible by changing the second line in question file. Number of answers must be 
between 2 and 9. Make sure that all questions within the same file have the same number of answers.

## License
Everyone is free to use the code as they wish. No credits needed.

## Project status
So far only one category, geography, is implemented but is not finished. Everything else (should) work fine.
