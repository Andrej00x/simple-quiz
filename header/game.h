#pragma once

#include <stdint.h>
#include <stdbool.h>

#define FILE_GEOGRAPHY "questions_geography.txt"

typedef enum {GEOGRAPHY = 1} category_t;

typedef struct {
	char question_text[100 + 1];
	char answers[9][25 + 1];
	char correct_answer[25 + 1];
} question_t;

typedef struct {
	uint16_t max_score;
	uint16_t player_score;
} score_t;

bool game_setup(category_t category, score_t* score, question_t** questions, uint16_t* questions_number, uint16_t* answers_number);
void game_play(score_t* score, question_t* questions, uint16_t questions_number, uint16_t answers_number);