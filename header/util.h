#pragma once

#include <stdbool.h>
#include <stdint.h>

bool replaceChar(char* str, char oldc, char newc);
bool arrayContains(uint16_t* arr, uint16_t element, uint16_t size);