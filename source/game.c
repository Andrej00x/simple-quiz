#include "game.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

/*
  Load the questions from a file and update max possible score.
  Returns true if loading is successful.
*/
bool game_setup(category_t category, score_t* score, question_t** questions, uint16_t* questions_number, uint16_t* answers_number) {
	FILE* fp;
	char buff[1024];
	uint16_t i;

	switch (category) {
	case GEOGRAPHY:
		strcpy_s(buff, sizeof(buff), FILE_GEOGRAPHY);
		break;
	default:
		printf("Somehow, you selected an incorrect category. Try restarting the quiz.\n");
		return false;
	}

	if (fopen_s(&fp, buff, "r") != 0) {
		fprintf(stderr, "Error opening questions file.\n");
		return false;
	}

	// ignore comments
	do {
		if (fgets(buff, sizeof(buff), fp) == NULL) {
			fprintf(stderr, "No questions in the file.\n");
			fclose(fp);
			return false;
		}
	} while (!strncmp(buff, "//", 2));

	// number of questions
	*questions_number = (uint16_t)atoi(buff);
	if (*questions_number == 0 || *questions_number > 85) {
		fprintf(stderr, "Cannot convert number of questions from a file to a positive integer smaller than 85.\n");
		fclose(fp);
		return false;
	}

	// number of answers
	if (fgets(buff, sizeof(buff), fp) == NULL) {
		fprintf(stderr, "Error reading from file. Expected number of answers but reached end of file.\n");
		fclose(fp);
		return false;
	}
	*answers_number = (uint16_t)atoi(buff);
	if (*answers_number < 2 || *answers_number > 9) {
		fprintf(stderr, "Cannot convert number of answers to integer between 2 and 9.\n");
		fclose(fp);
		return false;
	}

	score->max_score = *questions_number * (*answers_number - 1);
	score->player_score = 0;
	free(*questions);
	*questions = malloc(*questions_number * sizeof(question_t));

	// questions
	for (i = 0; i < *questions_number; i++) {
		if (fgets((*questions)[i].question_text, sizeof((*questions)[i].question_text), fp) == NULL) {
			fprintf(stderr, "Error reading from file. Check if a number of questions is correct.\n");
			fclose(fp);
			return false;
		}
		replaceChar((*questions)[i].question_text, '\n', '\0');
		for (int j = 0; j < *answers_number; j++) {
			if (fgets((*questions)[i].answers[j], sizeof((*questions)[i].answers[j]), fp) == NULL) {
				fprintf(stderr, "Error reading from a file. Expected reading an answer but reached end of the file.\n");
				fclose(fp);
				return false;
			}
			replaceChar((*questions)[i].answers[j], '\n', '\0');
		}
		strcpy_s((*questions)[i].correct_answer, sizeof((*questions)[i].answers[0]), (*questions)[i].answers[0]);
	}

	if (fgets(buff, sizeof(buff), fp) != NULL) {
		printf("Warning: End of file is not reached. Maybe the number of questions or answers in the file is not updated?\n");
	}

	fclose(fp);
	return true;
}

void game_play(score_t* score, question_t* questions, uint16_t questions_number, uint16_t answers_number) {
	uint16_t* generated_order_questions = malloc(questions_number * sizeof(uint16_t));
	uint16_t* generated_order_answers = malloc(answers_number * sizeof(uint16_t));
	uint16_t number;
	int16_t current_score = 0;
	uint16_t current_question;
	char choice;
	char max_answer[2];

	_itoa_s(answers_number, max_answer, _countof(max_answer), 10);

	// generate order of questions
	for (uint16_t i = 0; i < questions_number; i++)
		generated_order_questions[i] = UINT16_MAX;

	for (uint16_t i = 0; i < questions_number; i++) {
		number = (uint16_t)((float)rand() / (float)(RAND_MAX + 1) * questions_number);
		while (arrayContains(generated_order_questions, number, i + 1)) {
			if (++number >= questions_number) number = 0;
		}

		generated_order_questions[i] = number;
	}

	// start the quiz
	for (uint16_t i = 0; i < questions_number; i++) {
		current_question = generated_order_questions[i];

		//generate order of answers
		for (uint16_t j = 0; j < answers_number; j++)
			generated_order_answers[j] = UINT16_MAX;

		for (uint16_t j = 0; j < answers_number; j++) {
			number = (uint16_t)rand() % answers_number;
			while (arrayContains(generated_order_answers, number, answers_number)) {
				if (++number >= answers_number) number = 0;
			}

			generated_order_answers[j] = number;
		}


		printf("\nQuestion %hu:\n", i + 1);
		printf("%s\n", questions[current_question].question_text);
		for (uint16_t j = 0; j < answers_number; j++) {

			printf(" %hu) %s\n", j + 1, questions[current_question].answers[generated_order_answers[j]]);
		}

		printf("> ");
		choice = _getch();
		printf("%c\n", choice);

		while (choice < '1' || choice > max_answer[0]) {
			printf("Enter a valid number.\n> ");
			choice = _getch();
			printf("%c\n", choice);
		}
		number = atoi(&choice) - 1;

		if (strcmp(questions[current_question].answers[generated_order_answers[number]], questions[current_question].correct_answer)) {
			printf("Incorrect answer.\n");
			current_score--;
		}
		else {
			printf("Correct answer.\n");
			current_score += (answers_number - 1);
		}
	}

	current_score = max(current_score, 0);
	score->player_score = current_score;

	return;
}