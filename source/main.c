#include <stdio.h>
#include <conio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include "game.h"

int main() {
	char choice;
	score_t score;
	question_t* questions = NULL;
	uint16_t questions_number, answers_number;

	srand(time(0));
	rand();	// testing showed that first generated numbers after starting the program are
			// almost identical, so a dummy rand() is called

	printf("Welcome to the quiz. To answer a question, enter a number printed in front of desired answer.\n");
	do {
		printf("\nPlease enter a number to select a category:\n");
		printf(" 1 - Geography\n");
		printf(" 0 - Exit the game\n> ");
		choice = _getch();
		printf("%c\n", choice);

		while (choice < '0' || choice > '1') {
			printf("Enter a valid number.\n> ");
			choice = _getch();
			printf("%c\n", choice);
		}
		if (choice == '0') break;

		if (!game_setup(atoi(&choice), &score, &questions, &questions_number, &answers_number)) {
			printf("A quiz can not be started.\n");
			continue;
		}
		printf("Correct answer gives you %hu points, while incorrect reduces 1 point.\n", answers_number - 1);
		game_play(&score, questions, questions_number, answers_number);

		printf("\nYou scored %hu point%s out of maximum %hu.\n", score.player_score, score.player_score == 1 ? "" : "s", score.max_score);

	} while (choice != '0');

	free(questions);
	return 0;
}