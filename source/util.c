#include "util.h"
#include <string.h>

/*
  Replaces a first occurance of 'oldc' in 'str' with 'newc'.
  Returns true if a character is replaced.
*/
bool replaceChar(char* str, char oldc, char newc) {
	for (uint16_t i = 0; i < strlen(str); i++) {
		if (str[i] == oldc) {
			str[i] = newc;
			return true;
		}
	}

	return false;
}

bool arrayContains(uint16_t* arr, uint16_t element, uint16_t size) {
	for (uint16_t i = 0; i < size; i++) {
		if (arr[i] == element) return true;
	}

	return false;
}